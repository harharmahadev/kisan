<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User; 
use Illuminate\Support\Facades\Auth;
use Validator;


class AuthController extends Controller
{
	/**
     * Register
     */
    public function register(Request $request){

    	$user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            // 'password' => bcrypt($request->password)
            'password' => \Hash::make($request->password)
        ]);

        $token = $user->createToken('token')->accessToken;
 
        return response()->json(['token' => $token,'user' => $user], 200);

    }

    /**
     * Login
     */
    public function login(Request $request)
    {
        $data = [
            'email' => $request->email,
            'password' => $request->password
        ];
        if (auth()->attempt($data)) {
            $token = auth()->user()->createToken('token')->accessToken;
            return response()->json(['token' => $token], 200);
        } else {
            return response()->json(['error' => 'Unauthorised'], 401);
        }
    } 

    /**
     * Get User data
     */
    public function userInfo(Request $request)
    {
        $user = auth()->user();
        return response()->json(['user' => $user], 200);
    }


    /**
     * Update User profile
     */
    public function profileUpdate(Request $request, $id)
    {

        // $input = $request->all();
        // dd($request->address1);
        // $validator = Validator::make($input, [
        //     'name' => 'required',
        //     'email' => 'required'
        // ]);
   
        // if($validator->fails()){
        //     return response()->json(['user' => 'Validation Error'], 404);
        //     // return $this->sendError('Validation Error.', $validator->errors());       
        // }
        
        $user = User::find($id);
        $user->name = $request->name;
        $user->address1 = $request->address1;
        $user->address2 = $request->address2;
        $user->city = $request->city;
        $user->state = $request->state;        
        $user->dob = $request->dob;
        $user->gender = $request->gender;
        $user->land_size = $request->land_size;
        $user->family_name = $request->family_name;
        $user->irrigation = $request->irrigation;
        $user->farm_mechanization = $request->farm_mechanization;
        $user->land_state = $request->land_state;
        $user->land_district = $request->land_district;
        $user->land_taluk = $request->land_taluk;
        $user->mobile_number = $request->mobile_number;
        $user->occupation = $request->occupation;

        if($user->email !== $request->email)
        {
            $user->email = $request->email;
        }
        $user->save();
        return response()->json(['user' => $user], 200);
    }

}
