<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('imei')->nullable();
            $table->string('mobile_type')->nullable();
            $table->string('user_lat')->nullable();
            $table->string('user_long')->nullable();
            $table->string('token')->nullable();
            $table->string('app_version')->nullable();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('imei');
            $table->dropColumn('mobile_type');
            $table->dropColumn('user_lat');
            $table->dropColumn('user_long');
            $table->dropColumn('token');
            $table->dropColumn('app_version');
        });
    }
}
