<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UsersExtraFieldTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('address1')->nullable();
            $table->string('address2')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->string('dob')->nullable();
            $table->string('gender')->nullable();
            $table->string('land_size')->nullable();
            $table->string('family_name')->nullable();
            $table->string('irrigation')->nullable();
            $table->string('farm_mechanization')->nullable();
            $table->string('land_state')->nullable();
            $table->string('land_district')->nullable();
            $table->string('land_taluk')->nullable();
            $table->string('mobile_number')->nullable();
            $table->string('occupation')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('address1');
            $table->dropColumn('address2');
            $table->dropColumn('city');
            $table->dropColumn('state');
            $table->dropColumn('dob');
            $table->dropColumn('gender');
            $table->dropColumn('land_size');
            $table->dropColumn('family_name');
            $table->dropColumn('irrigation');
            $table->dropColumn('farm_mechanization');
            $table->dropColumn('land_state');
            $table->dropColumn('land_district');
            $table->dropColumn('land_taluk');
            $table->dropColumn('mobile_number');
            $table->dropColumn('occupation');
        });
    }
}
